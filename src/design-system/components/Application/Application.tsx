const Application = ({ name }: { name: string }) => (
  <div id={'single-spa-application:@dafexpert/' + name.toLocaleLowerCase()} />
);

export default Application;
