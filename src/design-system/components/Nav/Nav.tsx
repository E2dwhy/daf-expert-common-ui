import { Drawer, List, Stack } from '@mui/material';
import { Menu } from '../index';

import { Dispatch, SetStateAction } from 'react';
import { MenuProps } from '../Menu/Menu.types';

type WorkspaceNavProps = {
  open: boolean;
  handleOpen: Dispatch<SetStateAction<boolean>>;
  items: MenuProps[];
};

const Nav = ({ open, handleOpen, items }: WorkspaceNavProps) => (
  <Drawer
    variant="persistent"
    sx={{ zIndex: '100' }}
    open={open}
    PaperProps={{
      sx: { width: 220, top: 85, left: 50, background: '#fcfcfc !important', border: 'none' },
    }}
    onClose={() => handleOpen(false)}
  >
    <Stack direction="column" sx={{}}>
      <List>
        {items.map((item, idx) => (
          <Menu to={item.to} name={item.name} icon={item.icon} key={idx} />
        ))}
      </List>
    </Stack>
  </Drawer>
);

export default Nav;
