import { Drawer, List, Stack, SxProps, Theme } from '@mui/material';
import { Dispatch, SetStateAction } from 'react';
import { MenuGroupProps } from '../MenuGroup/MenuGroup.types';
import MenuGroup from '../MenuGroup/MenuGroup';

type WorkspaceNavProps = {
  isDesktop: boolean;
  open: boolean;
  handleOpen: Dispatch<SetStateAction<boolean>>;
  items: MenuGroupProps[];
  sx?: SxProps<Theme>;
};

const NavGroup = ({ open, handleOpen, items, sx, isDesktop }: WorkspaceNavProps) => {
  return (
    <Drawer
      variant="persistent"
      sx={{ zIndex: '100' }}
      open={open}
      PaperProps={{
        sx: {
          width: 220,
          left: isDesktop ? 50 : 0,
          //background: '#fcfcfc !important',
          ...sx,
        },
      }}
      onClose={() => handleOpen(false)}
    >
      <Stack direction="column" sx={{}}>
        <List>
          {items.map(({ name, items }, idx) => (
            <MenuGroup name={name} items={items} key={idx} />
          ))}
        </List>
      </Stack>
    </Drawer>
  );
};

export default NavGroup;
