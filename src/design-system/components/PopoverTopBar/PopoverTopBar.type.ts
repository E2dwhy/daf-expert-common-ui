import { MouseEvent, ReactNode } from 'react';

export type Profile = {
  id: string;
  accessToken: string;
  idToken?: string;
  connected?: boolean;
  structure: {
    firmName: string;
    picture: string;
  };
};

export type PopoverTopBarProps = {
  open: boolean;
  anchorEl: any;
  handleClose: () => void;
  header: ReactNode;
  content: ReactNode;
};
