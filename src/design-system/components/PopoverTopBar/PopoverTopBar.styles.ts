import styled from '@emotion/styled';
import { Menu, menuClasses } from '@mui/material';

export const StyledMenu = styled(Menu)({
  [`& .${menuClasses.root}`]: {
    height: 'inherit',
  },
  [`& .${menuClasses.paper}`]: {
    top: 50,
    flexGrow: 1,
  },
});

export const StyledPopoverTopBar = styled('div')({
  my: 1.5,
  px: 2.5,
  width: '100% ',
});
