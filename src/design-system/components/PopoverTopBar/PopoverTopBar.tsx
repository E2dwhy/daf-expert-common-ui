import { Divider, Menu } from '@mui/material';
import { ReactNode } from 'react';

import { PopoverTopBarProps } from './PopoverTopBar.type';

const HeaderPopover = ({ children }: { children: ReactNode }) => <div>{children}</div>;
const ContentPopover = ({ children }: { children: ReactNode }) => <div>{children}</div>;

const PopoverTopBar = ({ open, anchorEl, handleClose, header, content }: PopoverTopBarProps) => {
  return (
    <Menu
      id="lock-menu"
      anchorEl={anchorEl}
      open={open}
      onClose={handleClose}
      MenuListProps={{
        'aria-labelledby': 'lock-button',
        role: 'listbox',
      }}
    >
      <HeaderPopover>{header}</HeaderPopover>
      <Divider sx={{ borderStyle: 'dashed' }} />
      <ContentPopover>{content}</ContentPopover>
    </Menu>
  );
};

export default PopoverTopBar;
