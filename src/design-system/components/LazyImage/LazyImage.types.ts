import { SxProps } from '@mui/material';

export type LazyImageProps = {
  disabledEffect?: boolean;
  effect?: string;
  ratio?: '4/3' | '3/4' | '6/4' | '4/6' | '16/9' | '9/16' | '21/9' | '9/21' | '1/1';
  sx?: SxProps;
} & any;
