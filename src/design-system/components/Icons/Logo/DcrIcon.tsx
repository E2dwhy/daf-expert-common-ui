import Dcr from '../../../assets/images/img_dcr.png';
import { Box } from '@mui/material';

const DcrIcon = () => {
  return (
    <Box
      component="img"
      sx={{
        width: '25px',
        height: '100%',
      }}
      alt="Dcr"
      src={Dcr}
    />
  );
};

export default DcrIcon;
