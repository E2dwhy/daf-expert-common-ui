import Mrx from '../../../assets/images/img_mrx.png';
import { Box } from '@mui/material';

const MrxIcon = () => {
  return (
    <Box
      component="img"
      sx={{
        width: '25px',
        height: '100%',
      }}
      alt="Mrx"
      src={Mrx}
    />
  );
};

export default MrxIcon;
