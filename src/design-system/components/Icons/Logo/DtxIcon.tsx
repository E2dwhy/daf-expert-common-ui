import Dtx from '../../../assets/images/img_dtx.png';
import { Box } from '@mui/material';

const DtxIcon = () => {
  return (
    <Box
      component="img"
      sx={{
        width: '25px',
        height: '100%',
      }}
      alt="Dtx"
      src={Dtx}
    />
  );
};

export default DtxIcon;
