import Mn from '../../../assets/images/img_mn.png';
import { Box } from '@mui/material';

const MnIcon = () => {
  return (
    <Box
      component="img"
      sx={{
        width: '25px',
        height: '100%',
      }}
      alt="Mn"
      src={Mn}
    />
  );
};

export default MnIcon;
