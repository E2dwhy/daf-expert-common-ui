import { Box } from '@mui/material';
import { Hypnosis } from 'react-cssfx-loading';

import logoDafexpert from '../../../assets/images/logo_dafexpert.svg';

const LoadingScreen = () => {
  return (
    <Box
      data-testid="loadingContent"
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
      gap={4}
    >
      <img
        src={logoDafexpert}
        alt="logo"
        width={100}
        style={{ position: 'relative', left: '135px' }}
      />
      <Hypnosis color="#008773" width="7rem" height="7rem" duration="0.8s" />
    </Box>
  );
};

export default LoadingScreen;
