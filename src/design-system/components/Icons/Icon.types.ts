import { SxProps } from '@mui/material';

export type IconProps = {
  name: string;
  fontSize?: number | string | undefined;
  sx?: SxProps;
};
