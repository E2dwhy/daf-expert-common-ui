import { IconProps } from './Icon.types';
import React from 'react';
import * as Logo from './Logo';
import { AppConfig } from '../../config';
import { AiFillFolder, AiOutlineUser, AiFillFire } from 'react-icons/ai';
import { HiUsers, HiGift } from 'react-icons/hi';
import { RiSecurePaymentFill, RiMessage3Fill } from 'react-icons/ri';
import { FaQuestion, FaBell, FaLaptopCode } from 'react-icons/fa';
import { CgHomeAlt } from 'react-icons/cg';
import { TiUser } from 'react-icons/ti';
import { MdOutlinePayment } from 'react-icons/md';
import { GrAdd } from 'react-icons/gr';
import { IconType } from 'react-icons';
import { TbHeartHandshake } from 'react-icons/tb';

const _icons: { [k: string]: any } = {
  'digit-expert': 'digit',
  declarons: 'declarons',
  comptabilite: 'comptabilite',
  pgf: 'pgf',
  rh: 'rh',
  mn: 'mn',
  ecollect: 'ecollect',
  eboot: 'eboot',
  dafexpert: 'dafexpert',
  help: FaQuestion,
  promotion: HiGift,
  folders: AiFillFolder,
  collaborators: HiUsers,
  facturation: RiSecurePaymentFill,
  notification: FaBell,
  message: RiMessage3Fill,
  user: TiUser,
  payment: MdOutlinePayment,
  add: GrAdd,
  demo: FaLaptopCode,
  expert: AiOutlineUser,
  ambassador: TbHeartHandshake,
  structure: AiFillFire,
};

const Icon = ({ name, fontSize, sx }: IconProps) => {
  let icon = Logo[_icons[AppConfig[name]] as keyof typeof Logo];

  if (!icon) {
    icon = _icons[AppConfig[name]];
  }

  if (!icon) {
    icon = _icons[name];
  }

  return React.createElement<any | IconType>(icon, { fontSize, ...sx });
};

export default Icon;
