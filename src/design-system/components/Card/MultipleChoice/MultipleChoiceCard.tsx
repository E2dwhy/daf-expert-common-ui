import { Box, Grid } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { ChoiceCard } from '../Choice';
import { GoPlus } from 'react-icons/go';
import { Avatar } from '../../Avatar';
import { ReactNode } from 'react';

type ChoiceCard = {
  text: string;
  img: string;
  avatar: ReactNode;
  path: string;
};

type MultipleChoiceCardProps<T> = {
  name: string;
  items: T[];
  shouldAddNewItem: boolean;
  displayItem: (item: T) => ChoiceCard;
  onClickItem: (item: any) => void;
};

const MultipleChoiceCards = ({
  name,
  items,
  displayItem,
  shouldAddNewItem = false,
  onClickItem,
}: MultipleChoiceCardProps<any>) => {
  const navigate = useNavigate();

  return (
    <Grid
      container
      spacing={2}
      sx={{ marginTop: '20px' }}
      justifyContent="center"
      alignItems="stretch"
    >
      {items &&
        items.map((item: any, index) => {
          item = displayItem(item);
          return (
            <Grid item xs={6} md={2} key={index}>
              <Box
                onClick={() => (onClickItem ? onClickItem(item) : navigate(item.path))}
                sx={{ height: 1 }}
              >
                <ChoiceCard
                  text={item.text}
                  avatar={item.avatar || <Avatar src={item.img} alt={''} />}
                />
              </Box>
            </Grid>
          );
        })}

      {shouldAddNewItem && (
        <Grid item xs={6} md={2}>
          <Link to={'#'} style={{ textDecoration: 'none' }}>
            <Box sx={{ margin: 'auto', height: '100%' }}>
              <ChoiceCard text={`Ajouter un ${name}`} avatar={<GoPlus fontSize="xxx-large" />} />
            </Box>
          </Link>
        </Grid>
      )}
    </Grid>
  );
};

export default MultipleChoiceCards;
