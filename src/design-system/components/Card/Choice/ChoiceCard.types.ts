import { ReactNode } from 'react';
import { PaperProps } from '@mui/material';

export type ChoiceCardProps = {
  /**
   * card title
   */
  text: string | ReactNode;
  /**
   * image source
   */
  avatar: ReactNode;
  alt?: string;
} & PaperProps;
