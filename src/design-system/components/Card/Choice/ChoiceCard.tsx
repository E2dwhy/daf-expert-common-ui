import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import React from 'react';
import { ChoiceCardStyled } from './ChoiceCard.styles';
import { ChoiceCardProps } from './ChoiceCard.types';

const ChoiceCard = ({ text, avatar, alt, ...rest }: ChoiceCardProps) => (
  <ChoiceCardStyled elevation={0} {...rest}>
    <Box sx={{ marginBottom: '20px' }}>
      <Avatar alt={alt}>{avatar}</Avatar>
    </Box>
    <Typography
      textTransform="capitalize"
      fontWeight={500}
      // color={(theme) => theme.palette.secondary.contrastText}
      variant="body2"
      align="center"
    >
      {text}
    </Typography>
  </ChoiceCardStyled>
);

export default ChoiceCard;
