import Paper, { PaperProps } from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

export const ChoiceCardStyled = styled(Paper)<PaperProps>(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  alignItems: 'center',
  height: '100%',
  width: '100%',
  padding: '50px 30px',
  border: '1px solid #CFCFCF',
  borderRadius: 5,
  backgroundColor: '#FBFBFB',
  transition: theme.transitions.create(['border'], {
    duration: '0.2s',
    easing: 'ease-out',
    delay: '.3s',
  }),
  '& .MuiBox-root': {
    borderRadius: '50%',
    position: 'relative',
    padding: 4,
  },
  '& .MuiBox-root::after': {
    position: 'absolute',
    content: '\'\'',
    height: '100%',
    width: '100%',
    background: 'transparent',
    top: 0,
    left: 0,
    borderRadius: '50%',
    transition: theme.transitions.create(['all'], {
      duration: '0.3s',
      easing: 'ease-out',
      delay: '0.1s',
    }),
  },
  '& .MuiAvatar-circular': {
    background: theme.palette.common.white,
    color: theme.palette.primary.main,
    width: 64,
    height: 64,
    transition: theme.transitions.create(['all'], {
      duration: '0.3s',
      easing: 'ease-out',
    }),
  },
  '&:hover': {
    backgroundColor: theme.palette.secondary.main,
    cursor: 'pointer',
    border: 'none',
    '& .MuiBox-root::after': {
      border: `1px solid ${theme.palette.primary.main}`,
      transform: 'scale(1.15)',
    },
    '& .MuiAvatar-circular': {
      background: '#fff',
      transform: 'scale(1.1)',
    },
    '& .MuiTypography-body1': {
      color: theme.palette.primary.main,
    },
  },
}));
