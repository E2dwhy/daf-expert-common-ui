import { useState } from 'react';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Stack,
  Typography,
} from '@mui/material';
import { useFormikContext } from 'formik';

type Package = {
  title: string;
  amount: string;
  description: string;
  packageId: string;
};

type PackagesChoiceCardProps = {
  name: string;
  items: any[];
  displayItem: (item: Package) => Package;
};

const PackagesChoiceCard = (props: PackagesChoiceCardProps) => {
  const { name, items, displayItem } = props;

  const { setFieldValue } = useFormikContext();

  const [selected, setSelected] = useState<string>();

  const handleClick = (packageId: string) => () => {
    setSelected(packageId);
    setFieldValue(name, packageId);
  };

  return (
    <Box sx={{ mt: 5 }}>
      <Grid container spacing={{ xs: 4, md: 2 }} justifyContent="center">
        {items &&
          items.map((item, index) => {
            item = displayItem(item);

            return (
              <Grid item xs md={4} key={index}>
                <Card
                  elevation={0}
                  sx={{
                    m: 'auto',
                    borderRadius: '8px',
                    textAlign: 'center',
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    border: item.packageId === selected ? '1px solid green' : '0.5px solid #CFCFCF',
                  }}
                >
                  <CardContent>
                    <Stack
                      direction="column"
                      alignItems="center"
                      sx={{
                        width: 'max-content',
                        margin: 'auto',
                      }}
                    >
                      <Typography variant="subtitle2">{item.title}</Typography>
                      <Typography variant="h4">{item.amount}</Typography>
                    </Stack>

                    <Box
                      sx={{
                        mb: '15px',
                        lineHeight: 1,
                      }}
                    >
                      <Typography variant="caption">{item.description}</Typography>
                    </Box>
                  </CardContent>

                  <CardActions>
                    <Box sx={{ mb: '20px' }}>
                      <Button size="small" color="secondary" onClick={handleClick(item.packageId)}>
                        Sélectionner
                      </Button>
                    </Box>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
      </Grid>
    </Box>
  );
};

export default PackagesChoiceCard;
