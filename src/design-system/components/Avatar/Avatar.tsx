import { Typography, Avatar as MuiAvatar } from '@mui/material';

const Avatar = ({ src, alt }: { src: string; alt: string }) => (
  <MuiAvatar src={src} alt={alt} color="black">
    <Typography
      sx={{
        color: 'black',
      }}
    >
      {' '}
      K
    </Typography>
  </MuiAvatar>
);

export default Avatar;
