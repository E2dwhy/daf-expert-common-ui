import { ReactNode } from 'react';
import { PopoverProps } from '@mui/material';

export type Arrow =
  | 'top-left'
  | 'top-center'
  | 'top-right'
  | 'bottom-left'
  | 'bottom-center'
  | 'bottom-right'
  | 'left-top'
  | 'left-center'
  | 'left-bottom'
  | 'right-top'
  | 'right-center'
  | 'right-bottom';

export type MenuPopoverProps = {
  sx: object;
  children: ReactNode;
  disabledArrow: boolean;
  arrow: Arrow;
} & PopoverProps;
