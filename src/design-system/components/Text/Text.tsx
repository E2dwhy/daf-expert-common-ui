import { SxProps, Typography } from '@mui/material';
import { ReactNode } from 'react';

type Level = 'title' | 'subtitle' | 'caption';
type Variant = 'h3' | 'h4' | 'subtitle2' | 'caption';
type Align = 'center';

const _levels: Record<Level, { variant: Variant; fontWeight: number }> = {
  title: { variant: 'h3', fontWeight: 600 },
  caption: { variant: 'caption', fontWeight: 0 },
  subtitle: { variant: 'subtitle2', fontWeight: 400 },
};
const Text = ({
  children,
  level,
  align,
  onClick,
  sx,
}: {
  children: ReactNode;
  level: Level;
  align?: Align;
  onClick?: () => void;
  sx?: SxProps;
}) => {
  //const cursor = onClick ? {cursor: "pointer"}: {};

  return (
    <Typography
      variant={_levels[level].variant}
      sx={{
        fontWeight: _levels[level].fontWeight,
        //pb: 2,
        //...cursor
        ...sx,
      }}
      align={align}
      onClick={onClick}
    >
      {children}
    </Typography>
  );
};

export default Text;
