import { Stack } from '@mui/material';
import React from 'react';

type TabPanelProps = {
  children: React.ReactNode;
  index: string;
  value: string;
};

export default function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <Stack
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </Stack>
  );
}
