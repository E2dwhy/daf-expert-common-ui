import { AppBar, Box, Button, Container, Divider, Link } from '@mui/material';
import React, { ReactNode } from 'react';
import DafExpertIcon from '../../Icons/Logo/DafExpertIcon';
import LogoutIcon from './ic_logout.svg';

export const pxToRem = (value: number) => `${value / 16}rem`;

type BasicPageProps = {
  children: ReactNode;
  isAuthenticated: boolean;
  onLogoutClick?: () => void;
  onHomePageClick?: () => void;
};

const BasicPage = ({
  children,
  isAuthenticated = false,
  onLogoutClick,
  onHomePageClick,
}: BasicPageProps) => (
  <>
    <Box>
      <Container>
        <AppBar position="static" elevation={0} sx={{ backgroundColor: '#fff' }}>
          <Box
            sx={{
              backgroundColor: '#fff',
              marginTop: '20px',
              marginBottom: '20px',
              zIndex: 0,
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <DafExpertIcon />
            {isAuthenticated ? (
              <Button
                color="secondary"
                variant="contained"
                onClick={onLogoutClick}
                endIcon={<img src={LogoutIcon} alt="logo" />}
              >
                Déconnexion
              </Button>
            ) : (
              <Button
                color="secondary"
                variant="contained"
                onClick={() => document.location.assign(window.location.origin)}
              >
                Page d'accueil
              </Button>
            )}
          </Box>
        </AppBar>
        <Divider />
      </Container>
    </Box>

    <Box
      sx={{
        flexGrow: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: pxToRem(5),
        marginBottom: pxToRem(50),
        padding: '20px',
      }}
    >
      <Container maxWidth="lg">{children}</Container>
    </Box>
  </>
);

export default BasicPage;
