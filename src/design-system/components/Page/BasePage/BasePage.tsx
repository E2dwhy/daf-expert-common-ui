import { Grid } from '@mui/material';
import { ReactNode } from 'react';

const BasePage = ({ children }: { children: ReactNode }) => (
  <Grid container sx={{ minHeight: '80vh' }}>
    {children}
  </Grid>
);

export default BasePage;
