import { Stack } from '@mui/material';
import { PageProps } from './FeaturePage.types';
import FeaturePageNav from './FeaturePageNav';
import FeaturePageContent from './FeaturePageContent';
import FeaturePageTitle from './FeaturePageTitle';
import { useAppTheme } from '../../../hooks';
import localstorage from 'localforage';
import { useMemo } from 'react';
import { Theme } from '../../../themes';

const FeaturePage = ({ name, title, navItems, links, action, children }: PageProps) => {
  const { theme, setTheme } = useAppTheme();
  const titleBarHeight = 70;

  useMemo(() => {
    localstorage.getItem<{ app: Theme }>('appTheme').then((_appTheme) => {
      setTheme({ ...theme, ..._appTheme });
    });
  }, []);

  return (
    <Stack flexDirection="column">
      <FeaturePageTitle
        name={name}
        title={title}
        action={action}
        links={links}
        sx={{
          position: 'fixed',
          width: 1,
          height: titleBarHeight,
        }}
      />

      {navItems && (
        <FeaturePageNav
          isDesktop={theme.isDesktop}
          navItems={navItems}
          sx={{
            top: theme.appBar.height + titleBarHeight,
            border: 'none',
          }}
        />
      )}

      <FeaturePageContent top={titleBarHeight} navItems={navItems}>
        {children}
      </FeaturePageContent>
    </Stack>
  );
};

export default FeaturePage;
