import { PageNavProps } from './FeaturePage.types';
import { NavGroup } from '../../Nav';

const FeaturePageNav = ({ navItems, sx, isDesktop }: PageNavProps) => (
  <NavGroup
    isDesktop={isDesktop}
    sx={sx}
    items={navItems}
    open={true}
    handleOpen={() => console.log('hallo')}
  />
);

export default FeaturePageNav;
