import { ReactNode } from 'react';
import { MenuGroupProps } from '../../MenuGroup/MenuGroup.types';
import { SxProps, Theme } from '@mui/material';
import { LinkProps } from '../../HeaderBreadcrumbs/Breadcrumbs';

export type PageProps = {
  name: string;
  title: string;
  children: ReactNode;
  navItems: MenuGroupProps[];
  level?: string;
  links?: LinkProps[];
  action?: ReactNode;
  isDesktop: boolean;
};

export type PageNavProps = {
  isDesktop: boolean;
  navItems: PageProps['navItems'];
  sx?: SxProps<Theme>;
};

export type PageContentProps = {
  children: PageProps['children'];
  navItems: PageProps['navItems'];
  top: number;
};

export type PageTitleProps = {
  name: string;
  title: PageProps['title'];
  links?: LinkProps[];
  action?: ReactNode;
  sx?: SxProps<Theme>;
};
