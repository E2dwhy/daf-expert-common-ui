import { Box, Stack } from '@mui/material';
import { PageTitleProps } from './FeaturePage.types';
import { Text } from '../../Text';
import { Icons } from '../../Icons';
import { HeaderBreadcrumbs } from '../../HeaderBreadcrumbs';

const FeaturePageTitle = ({ name, title, links, action, sx }: PageTitleProps) => (
  <Box sx={{ display: 'flex', ...sx }}>
    <Stack direction="column" justifyContent="center" sx={{ width: 0.96 }}>
      {links && action && (
        <HeaderBreadcrumbs title={title} name={name} links={links} action={action} />
      )}
    </Stack>
  </Box>
);

export default FeaturePageTitle;
