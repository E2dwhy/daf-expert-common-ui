import { PageContentProps } from './FeaturePage.types';
import { Box } from '@mui/material';

const FeaturePageContent = ({ children, navItems, top }: PageContentProps) => (
  <Box
    sx={{
      marginTop: `${top}px`,
      marginLeft: navItems ? '230px' : 0,
      marginRight: '10px',
    }}
  >
    {children}
  </Box>
);

export default FeaturePageContent;
