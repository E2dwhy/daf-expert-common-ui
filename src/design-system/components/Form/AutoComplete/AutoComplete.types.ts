import { FieldProps } from 'formik';

export type AutoCompleteProps = {
  label: string;
  options?: Array<{ label: string; value: number }>;
  isLoading?: boolean;
  placeholder?: string;
} & FieldProps;
