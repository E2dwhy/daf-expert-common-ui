import { getIn } from 'formik';
import MuiAutocomplete from '@mui/material/Autocomplete';
import { CircularProgress, createFilterOptions, TextField } from '@mui/material';
import { AutoCompleteProps } from './AutoComplete.types';

const AutoComplete = ({
  field,
  form: { setFieldValue, touched, errors },
  label,
  options,
  isLoading,
  placeholder,
  ...props
}: AutoCompleteProps) => {
  const filterOptions = createFilterOptions({
    matchFrom: 'start',
    limit: 500,
  });
  const errorText = getIn(touched, field?.name) && getIn(errors, field?.name);

  return (
    <MuiAutocomplete
      {...props}
      {...field}
      filterOptions={filterOptions}
      options={options ?? []}
      getOptionLabel={(option) => option?.label ?? option}
      loading={isLoading}
      value={field?.value}
      onChange={(e, value) => {
        if (value !== null) {
          setFieldValue(field.name, value);
        } else {
          setFieldValue(field.name, '');
        }
      }}
      renderInput={(props) => (
        <>
          <TextField
            {...props}
            label={label}
            helperText={errorText?.value || errorText}
            error={!!errorText}
            placeholder={placeholder}
            InputProps={{
              ...props.InputProps,
              endAdornment: (
                <>
                  {isLoading ? <CircularProgress color="primary" size={20} /> : null}
                  {props.InputProps.endAdornment}
                </>
              ),
            }}
          />
        </>
      )}
    />
  );
};

export default AutoComplete;
