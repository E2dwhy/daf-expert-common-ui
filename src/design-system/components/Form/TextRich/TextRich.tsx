import React from 'react';
import 'quill-mention';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.core.css';
import 'quill-mention/dist/quill.mention.css';

import { formats, textRichConfig } from './textRich.constants';
import { FieldProps } from 'formik';
import { Grid } from '@mui/material';

const TextRichCustom = React.forwardRef(
  ({ field, readonly, rest }: Partial<FieldProps> & any, ref) => {
    return (
      <Grid container spacing={2}>
        <Grid item md={12} className="quillHeight">
          <ReactQuill
            ref={ref}
            theme="snow"
            modules={textRichConfig}
            formats={formats}
            value={field.value}
            readOnly={readonly}
            className="cadreTextRichQuill"
            onChange={field.onChange(field.name)}
            {...rest}
            style={{ height: '100%' }}
          />
        </Grid>
      </Grid>
    );
  }
);

TextRichCustom.displayName = 'TextRichCustom';
export default TextRichCustom;
