import { TableCell, TableRow } from '@mui/material';
import { EmptyContent } from '../EmptyContent';

const TableNoData = ({ isNotFound }: { isNotFound: boolean }) => (
  <>
    {isNotFound ? (
      <TableRow>
        <TableCell colSpan={9}>
          <EmptyContent
            title="Pas de données"
            sx={{
              '& span.MuiBox-root': { height: 160 },
            }}
          />
        </TableCell>
      </TableRow>
    ) : (
      <TableRow>
        <TableCell colSpan={9} sx={{ p: 0 }} />
      </TableRow>
    )}
  </>
);

export default TableNoData;
