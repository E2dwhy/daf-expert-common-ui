import { TableCell, TableRow } from '@mui/material';

type TableEmptyRowsProps = {
  emptyRows: number;
  height: number;
};

const TableEmptyRows = ({ emptyRows, height }: TableEmptyRowsProps) => {
  if (!emptyRows) {
    return null;
  }

  return (
    <TableRow
      sx={{
        ...(height && {
          height: height * emptyRows,
        }),
      }}
    >
      <TableCell colSpan={9} />
    </TableRow>
  );
};

export default TableEmptyRows;
