import { IconButton } from '@mui/material';
import { FiMoreVertical } from 'react-icons/fi';
import { MenuPopover } from '../MenuPopover';
import { ReactNode } from 'react';

type TableMoreMenuProps = {
  actions: ReactNode;
  open: Element | ((element: Element) => Element) | null | undefined;
  onClose: () => void;
  onOpen: any;
};

const TableMoreMenu = ({ actions, open, onClose, onOpen }: TableMoreMenuProps) => {
  return (
    <>
      <IconButton onClick={onOpen}>
        <FiMoreVertical width={20} height={20} />
      </IconButton>

      <MenuPopover
        open={Boolean(open)}
        anchorEl={open}
        onClose={onClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        arrow="right-top"
        sx={{
          mt: -1,
          width: 160,
          '& .MuiMenuItem-root': {
            px: 1,
            typography: 'body2',
            borderRadius: 0.75,
            '& svg': { mr: 2, width: 20, height: 20 },
          },
        }}
        disabledArrow
      >
        {actions}
      </MenuPopover>
    </>
  );
};

export default TableMoreMenu;
