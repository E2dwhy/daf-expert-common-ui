import { Box, Checkbox, TableCell, TableHead, TableRow, TableSortLabel } from '@mui/material';

const visuallyHidden = {
  border: 0,
  margin: -1,
  padding: 0,
  width: '1px',
  height: '1px',
  overflow: 'hidden',
  position: 'absolute',
  whiteSpace: 'nowrap',
  clip: 'rect(0 0 0 0)',
};

export type headLabel = {
  id?: string;
  label?: string | undefined;
  align?: 'left' | 'right' | 'center' | 'justify' | 'inherit' | undefined;
  width?: number | undefined;
  minWidth?: number | undefined;
};

type TableHeadCustomProps = {
  onSort: (arg?: any) => void;
  orderBy: string;
  headLabel: Partial<headLabel>[];
  rowCount: number;
  numSelected: number;
  onSelectAllRows: (arg?: any) => void;
  order: 'asc' | 'desc';
  sx?: object;
};

const TableHeadCustom = (props: TableHeadCustomProps) => {
  const {
    order,
    orderBy,
    rowCount = 0,
    headLabel,
    numSelected = 0,
    onSort,
    onSelectAllRows,
    sx,
  } = props;

  return (
    <TableHead sx={sx}>
      <TableRow>
        {onSelectAllRows && (
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={(event) => onSelectAllRows(event.target.checked)}
            />
          </TableCell>
        )}

        {headLabel.map((headCell, idx) => (
          <TableCell
            key={idx}
            align={headCell.align || 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={{
              width: headCell.width,
              minWidth: headCell.minWidth,
              background: '#F2FEF6',
            }}
          >
            {onSort ? (
              <TableSortLabel
                hideSortIcon
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={() => onSort(headCell.id)}
                sx={{ textTransform: 'capitalize' }}
              >
                {headCell.label}

                {orderBy === headCell.id ? (
                  <Box sx={{ ...visuallyHidden }}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </Box>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeadCustom;
