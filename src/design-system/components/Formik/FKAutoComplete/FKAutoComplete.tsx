import { Field, FieldAttributes } from 'formik';

import { AutoCompleteProps } from '../../Form/AutoComplete/AutoComplete.types';
import { AutoComplete } from '../../Form/AutoComplete';

const FKAutoComplete = <T extends AutoCompleteProps>(props: FieldAttributes<T>) => (
  <Field component={AutoComplete} {...props} />
);

export default FKAutoComplete;
