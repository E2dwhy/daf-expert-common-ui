import { Field, FieldAttributes } from 'formik';
import { TextRichdProps } from '../../Form/TextRich/TextRich.type';

import TextRich from '../../Form/TextRich/TextRich';

const FKTextRich = <T extends TextRichdProps>(props: FieldAttributes<T>) => (
  <Field component={TextRich} {...props} />
);

export default FKTextRich;
