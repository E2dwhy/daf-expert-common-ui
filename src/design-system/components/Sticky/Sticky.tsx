import { Box } from '@mui/material';
import React from 'react';

const Sticky = ({ children }: { children: React.ReactNode }) => (
  <Box
    sx={{
      position: 'sticky',
      top: 0,
      maxHeight: 220,
      marginTop: 2,
      marginBottom: 5,
      overflowX: 'auto',
    }}
  >
    {children}
  </Box>
);

export default Sticky;
