import { LoadingButton } from '@mui/lab';

type PrintFileProps = {
  label: string;
  actions: (arg?: any) => Promise<any>;
};

const downloadPdfFile = (url: string) => {
  const link = document.createElement('a');
  link.href = url;
  link.target = '_blank';

  link.setAttribute('download', url);
  document.body.appendChild(link);
  link.click();
  link?.parentNode?.removeChild(link);
};

const PrintFile = ({ label, actions }: PrintFileProps) => {
  const getPrintFile = async () => {
    const response = await actions().then((res: any) => res.data);
    const path = response?.data?.path;

    if (response) {
      downloadPdfFile(path);
    }
  };

  return <LoadingButton onClick={getPrintFile}>{label}</LoadingButton>;
};

export default PrintFile;
