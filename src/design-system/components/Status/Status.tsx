import { Typography } from '@mui/material';
import Dot from './Dot';
import { STATUS_LABEL, StatusProps } from './Status.types';

const Status = ({ status = 'IN_PROGRESS', title, ...rest }: StatusProps) => (
  <Typography
    {...rest}
    variant="caption"
    display="flex"
    alignItems="center"
    gap={1}
    fontWeight={600}
    color="#545454"
    textTransform="capitalize"
  >
    <Dot status={status} />
    {STATUS_LABEL[title]}
  </Typography>
);

export default Status;
