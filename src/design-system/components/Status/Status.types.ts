export const STATUS_LABEL: { [k: string]: string } = {
  IN_PROGRESS: 'En cours',
  DEACTIVATE: 'Désactivé',
  DISABLED: 'Désactivé',
  ACTIVATE: 'Activé',
  ENABLED: 'Activé',
  VALIDATE: 'Valide',
  SUCCESS: 'Succès',
  INCOMPLETE: 'Incomplet',
  WARNING: 'avertissement',
  ARCHIVED: 'Archivé',
  EXPIRED: 'Expiré',
  OUTSTANDING: 'Hors champ',
  REFUSE: 'Refusé',
  ACCEPT: 'Accepté',
  DRAFT: 'Brouillon',
  IN_RUNNING: 'En cours',
  BROUILLON: 'Brouillon',
  SAVED: 'Enregistré',
  SENT: 'Envoyé',
  ACCEPTED: 'Accepté',
  REFUSED: 'Refusé',
  ORDERED: 'Ordonné',
  DELIVERED: 'BL',
  INVOICED: 'Facturé',
  PAID: 'Payé',
  UNPAID: 'Impayé',
  RESPONSE: 'Reponse',
  CASHIERED: 'Encaissé',
  UNCASHIERED: 'Non Encaissé',
  PAYING: 'En Règlement',
  WAITING: 'En Attente',
  RELAUNCHED: 'Relancé',
  IN_WARNING: 'En attente',
  IN_WAITING: 'En cours',
  MAINTAINED: 'Maintenu',
  NO_MAINTAINED: 'Non maintenu',
  STATE: 'PM',
  'ML ACCEPT': 'ML ACCEPT',
  'MO REFUSE': 'OM Refusé',
  'LM ACCEPT': 'LM ACCEPT',
  INITIAL_RUNNING: 'En cours',
  'Pas de PM': 'PAS DE PM',
};

export enum STATUS_COLOR {
  IN_PROGRESS = '#FFA407',
  DEACTIVATE = '#FF0707',
  DISABLED = '#FF0707',
  ACTIVATE = '#73DC96',
  ENABLED = '#73DC96',
  VALIDATE = '#73DC96',
  SUCCESS = '#73DC96',
  INCOMPLETE = '#037FFC',
  WARNING = '#FF0707',
  ARCHIVED = '#FFBFBF',
  EXPIRED = '#A507FF',
  OUTSTANDING = '#FFF23D',
  REFUSE = '#FF0707',
  DRAFT = '#FFF23D',
  ACCEPT = '#73DC96',
  IN_RUNNING = '#FFA407',
  BROUILLON = '#e97c00',
  SAVED = '#0f9db4',
  SENT = '#0fb436',
  ACCEPTED = '#0abc94',
  REFUSED = '#bc0a0a',
  ORDERED = '#6eac00',
  DELIVERED = '#6eac00',
  INVOICED = '#047d28',
  PAID = '#4a8b7d',
  UNPAID = '#c0c852',
  RESPONSE = '#848a59',
  CASHIERED = '#6c9e57',
  UNCASHIERED = '#c0c852',
  PAYING = '#848a59',
  WAITING = '#e97c00',
  IN_WAITING = '#e97c00',
  RELAUNCHED = '#64689F',
  MAINTAINED = '#73DC96',
  NO_MAINTAINED = '#FF0707',
  STATE = '#0abc94',
  'MO REFUSE' = '#FF0707',
  'ML ACCEPT' = '#73DC96',
  'LM ACCEPT' = '#73DC96',
  INITIAL_RUNNING = '#e97c00',
  'Pas de PM' = '#e97c00',
}

export type StatusState = keyof typeof STATUS_COLOR;

export type StatusProps = {
  status: StatusState;
  title: string;
};
