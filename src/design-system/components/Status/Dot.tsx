import styled from '@emotion/styled';
import { STATUS_COLOR, StatusState } from './Status.types';

type DotProps = {
  status: StatusState;
};

const Dot = styled.span<DotProps>(({ status }) => ({
  display: 'inline-block',
  width: 8,
  height: 8,
  borderRadius: '50%',
  color: STATUS_COLOR[status],
  background: STATUS_COLOR[status],
}));

export default Dot;
