import { MenuProps } from '../Menu/Menu.types';

export type MenuGroupProps = {
  name: string;
  items: MenuProps[];
};
