import { ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { MenuGroupProps } from './MenuGroup.types';

const MenuGroup = ({ name, items }: MenuGroupProps) => {
  const navigate = useNavigate();

  return (
    <>
      <Typography fontSize="0.88rem">{name}</Typography>
      {items.map(({ name, icon, to }) => (
        <ListItemButton key={name} sx={{ fontSize: '0.5rem' }} onClick={() => navigate(to)}>
          <ListItemIcon sx={{ fontSize: '1.3rem', minWidth: 30 }}>{icon}</ListItemIcon>
          <ListItemText
            sx={{
              '.MuiTypography-root': {
                fontSize: '0.78rem',
              },
            }}
          >
            {name}
          </ListItemText>
        </ListItemButton>
      ))}
    </>
  );
};

export default MenuGroup;
