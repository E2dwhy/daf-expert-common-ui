import { ReactNode } from 'react';

export type MenuProps = {
  name: string;
  icon: ReactNode;
  to: string;
};

export type MenuItem = {
  name: string;
  icon: ReactNode;
};
