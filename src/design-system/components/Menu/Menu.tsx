import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { MenuProps } from './Menu.types';

const Menu = ({ name, icon, to }: MenuProps) => {
  const navigate = useNavigate();

  return (
    <>
      <ListItemButton sx={{ fontSize: '0.5rem' }} onClick={() => navigate(to)}>
        <ListItemIcon sx={{ minWidth: 30 }}>{icon}</ListItemIcon>
        <ListItemText
          sx={{
            '.MuiTypography-root': {
              fontSize: '0.88rem',
            },
          }}
        >
          {name}
        </ListItemText>
      </ListItemButton>
    </>
  );
};
export default Menu;
