import { forwardRef } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Link as MuiLink } from '@mui/material';
import { LinkRefProps } from './Link.types';
import { useFolderContext } from '../../hooks';

const Link = forwardRef<HTMLAnchorElement, LinkRefProps>((props, ref) => {
  const { href, ...rest } = props;
  const { dossier } = useFolderContext();
  const search = dossier && '?' + 'dossier' + dossier.id;

  return <MuiLink component={RouterLink} ref={ref} to={href + search} {...rest} />;
});

Link.displayName = 'Link';

export default Link;
