import { LinkProps } from 'react-router-dom';

export type LinkRefProps = Omit<LinkProps, 'to'> & { href: LinkProps['to'] };
