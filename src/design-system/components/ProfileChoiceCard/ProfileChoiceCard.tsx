import { PaperProps } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import React, { ReactNode } from 'react';

import StyledChoiceCard from './StyledChoiceCard';

export type ChoiceCardProps = {
  /**
   * card title
   */
  text: string;
  /**
   * image source
   */
  avatar: ReactNode;
} & PaperProps;

export default function ChoiceCard({ text, avatar, ...rest }: ChoiceCardProps) {
  return (
    <StyledChoiceCard elevation={0} {...rest}>
      <Box sx={{ marginBottom: '20px' }}>
        <Avatar alt={text}>{avatar}</Avatar>
      </Box>
      <Typography
        textTransform="capitalize"
        fontWeight={500}
        color={(theme) => theme.palette.secondary.contrastText}
        variant="body2"
        align="center"
      >
        {text}
      </Typography>
    </StyledChoiceCard>
  );
}
