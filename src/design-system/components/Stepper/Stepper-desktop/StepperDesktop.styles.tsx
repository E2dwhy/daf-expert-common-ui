import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { styled } from '@mui/material/styles';

export const ConnectorWrapper = styled(StepConnector)(({ theme }) => ({
  position: 'relative',
  top: '23px',
  left: '-7.4rem',
  [`& .${stepConnectorClasses.line}`]: {
    border: 0,
    height: 5,
    borderRadius: 5,
    width: 10,
    position: 'absolute',
  },
}));
