import { ConnectorWrapper } from './StepperDesktop.styles';
import { Step, Stepper, StepLabel, stepLabelClasses } from '@mui/material';

type StepperDesktopPops = { activeStep: number; steps: string[] };

const StepperDesktop = ({ activeStep, steps }: StepperDesktopPops) => (
  <Stepper
    activeStep={activeStep}
    sx={{
      //overflowX: 'scroll',
      border: '1px solid #E3E3E3',
      borderRadius: '5px',
      padding: '20px 200px 5px 200px',
      justifyContent: 'center',
      [`& .${stepLabelClasses.labelContainer} .${stepLabelClasses.active}`]: {
        backgroundColor: (theme) => theme.palette.secondary.main,
        padding: '5px',
        borderRadius: '5px',
      },
    }}
    connector={<ConnectorWrapper />}
  >
    {steps.map((label, index) => (
      <Step key={label} sx={{ mb: '15px' }}>
        <StepLabel>{label}</StepLabel>
      </Step>
    ))}
  </Stepper>
);

export default StepperDesktop;
