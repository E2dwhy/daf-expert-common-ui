import { FormikHelpers } from 'formik';

export type StepperFormsProps<T, D> = {
  isDesktop: boolean;
  defaultActiveStep: number;
  steps: string[];
  stepForms: JSX.Element[];
  onSubmitStepForms: Array<(values: T) => void>;
  inputValues: T;
  onSubmitStepperForms: (values: T, helpers?: FormikHelpers<T>) => void;
  labelSubmitStepper: string;
  validationsSchema?: D | (() => D);
};

export type StepperActionProps<T, D> = {
  values: T;
  isSubmitting: boolean;
  errors?: any;
  isValid?: boolean;
  validateForm?: any;
  setErrors?: any;
  steps: StepperFormsProps<T, D>['steps'];
  activeStep: number;
  goToNextStep: (values: T) => void;
  goToPreviousStep: (values: T) => void;
  onSubmitStepForms: StepperFormsProps<T, D>['onSubmitStepForms'];
  labelSubmitStepper: StepperFormsProps<T, D>['labelSubmitStepper'];
};
