import React from 'react';

const useStepperForms = ({
  steps,
  defaultActiveStep,
}: {
  defaultActiveStep: number;
  steps: string[];
}) => {
  const [activeStep, setActiveStep] = React.useState<number>(defaultActiveStep);
  const [completed, setCompleted] = React.useState<{ [k: number]: boolean }>({});

  const totalSteps = () => steps.length;
  const completedSteps = () => Object.keys(completed).length;
  const allStepsCompleted = () => completedSteps() === totalSteps();
  const isLastStep = activeStep === steps.length - 1;

  const goToNextStep = (values: any) => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;

    const newActiveStep =
      isLastStep && !allStepsCompleted()
        ? steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setCompleted(newCompleted);
    setActiveStep(newActiveStep);
  };

  const goToPreviousStep = (values: any) => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return { goToNextStep, goToPreviousStep, activeStep, isLastStep };
};

export default useStepperForms;
