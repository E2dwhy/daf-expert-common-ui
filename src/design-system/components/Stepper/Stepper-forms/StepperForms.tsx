import Stepper from '../Stepper';
import React from 'react';
import useStepperForms from './useStepperForms';
import { FKProvider } from '../../Formik';
import { FormikValues } from 'formik';
import { StepperFormsProps } from './StepperForms.types';
import StepperFormsActions from './StepperFormsActions';

const StepperForms = <T extends FormikValues>({
  isDesktop = true,
  defaultActiveStep = 0,
  steps,
  stepForms,
  inputValues,
  onSubmitStepForms,
  labelSubmitStepper,
  onSubmitStepperForms,
  validationsSchema,
}: StepperFormsProps<T, any>) => {
  const { goToNextStep, goToPreviousStep, activeStep } = useStepperForms({
    steps,
    defaultActiveStep,
  });

  return (
    <>
      <Stepper isDesktop={isDesktop} activeStep={activeStep} steps={steps} />
      <FKProvider
        value={inputValues}
        onSubmit={onSubmitStepperForms}
        validationsSchema={validationsSchema}
      >
        {React.cloneElement(stepForms[activeStep] || <></>, {
          actions: (
            values: T,
            isSubmitting: boolean,
            errors?: any,
            validateForm?: any,
            setErrors?: any,
            isValid?: boolean
          ) =>
            StepperFormsActions({
              values,
              isSubmitting,
              errors,
              isValid,
              validateForm,
              setErrors,
              steps,
              activeStep,
              goToNextStep,
              goToPreviousStep,
              onSubmitStepForms,
              labelSubmitStepper,
            }),
        })}
      </FKProvider>
    </>
  );
};

export default StepperForms;
