import { StepperActionProps } from './StepperForms.types';
import { Box, Button } from '@mui/material';
import React from 'react';

const Actions = <T, D>({
  values,
  isSubmitting,
  errors,
  isValid,
  validateForm,
  setErrors,
  steps,
  activeStep,
  goToNextStep,
  goToPreviousStep,
  onSubmitStepForms,
  labelSubmitStepper,
}: StepperActionProps<T, D>) => {
  return (
    <Box
      sx={{
        pt: 5,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {activeStep !== 0 && (
        <Button disabled={isSubmitting} onClick={() => goToPreviousStep(values)}>
          Précedent
        </Button>
      )}

      {activeStep < steps.length - 1 && (
        <Button
          type="submit"
          onClick={() => {
            onSubmitStepForms &&
              onSubmitStepForms[activeStep] &&
              onSubmitStepForms[activeStep](values);
            console.log(isValid);
            if (isValid) {
              goToNextStep(values);
            }
          }}
        >
          Suivant
        </Button>
      )}

      {activeStep === steps.length - 1 && (
        <Button type="submit" disabled={isSubmitting}>
          {labelSubmitStepper}
        </Button>
      )}
    </Box>
  );
};

export default Actions;
