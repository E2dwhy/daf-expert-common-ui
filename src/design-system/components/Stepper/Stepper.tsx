import StepperMobile from './Stepper-mobile/StepperMobile';
import StepperDesktop from './Stepper-desktop/StepperDesktop';

type StepperProps = { isDesktop: boolean; activeStep: number; steps: string[] };

const Stepper = ({ isDesktop, activeStep, steps }: StepperProps) => (
  <>{isDesktop ? <StepperDesktop activeStep={activeStep} steps={steps} /> : <StepperMobile />}</>
);
export default Stepper;
