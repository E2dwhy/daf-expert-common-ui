import { Box, Stack } from '@mui/material';
import { ReactNode } from 'react';
import Breadcrumbs, { LinkProps } from './Breadcrumbs';
import { Icons } from '../Icons';

type HeaderBreadcrumbsProps = {
  title: string;
  name: string;
  links: LinkProps[];
  action?: ReactNode;
  sx?: object;
};

const HeaderBreadcrumbs = ({ title, name, links, action, ...other }: HeaderBreadcrumbsProps) => {
  return (
    <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ ml: 2 }}>
      <Stack
        direction="row"
        spacing={1}
        sx={{
          ml: 2,
          fontSize: '1.7em',
          display: 'flex',
          alignItems: 'center',
          color: '#6a6d73',
        }}
      >
        <Icons name={name} />
      </Stack>
      <Box sx={{ flexGrow: 1 }}>
        <Breadcrumbs links={[{ name: title }, ...links]} {...other} />
      </Box>

      {action && <Box sx={{ flexShrink: 0 }}>{action}</Box>}
    </Stack>
  );
};

export default HeaderBreadcrumbs;
