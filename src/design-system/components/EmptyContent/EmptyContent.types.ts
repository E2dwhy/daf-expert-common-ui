export type EmptyContentProps = {
  title: string;
  img?: string;
  description?: string;
  sx?: any;
};
