import { Typography } from '@mui/material';
import Nodata from './no_data.svg';
import { EmptyContentProps } from './EmptyContent.types';
import { StyledEmptyContent } from './EmptyContent.styles';
import { LazyImage } from '../LazyImage';

const EmptyContent = ({ title, description, img, ...other }: EmptyContentProps) => {
  return (
    <StyledEmptyContent {...other}>
      <LazyImage
        disabledEffect
        visibleByDefault
        alt="empty content"
        src={img || Nodata}
        sx={{ height: 240, mb: 3 }}
      />

      <Typography variant="h5" gutterBottom>
        {title}
      </Typography>

      {description && (
        <Typography variant="body2" sx={{ color: 'text.secondary' }}>
          {description}
        </Typography>
      )}
    </StyledEmptyContent>
  );
};

export default EmptyContent;
