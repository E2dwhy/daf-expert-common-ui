import { CustomNoRowsOverlay, StyledDataGrid } from './DataGrid.styles';
import { DataGridProps } from '@mui/x-data-grid';
import { forwardRef } from 'react';
import { DATAGRID_HEADER_HEIGHT, localText, PAGE_SIZE } from './DataGrid.constants';

const DataGrid = (props: DataGridProps, ref: any) => (
  <StyledDataGrid
    checkboxSelection
    localeText={localText}
    columnBuffer={3}
    columnThreshold={3}
    components={{
      NoRowsOverlay: CustomNoRowsOverlay,
    }}
    autoPageSize
    hideFooterPagination
    disableColumnMenu
    disableSelectionOnClick
    hideFooter
    headerHeight={DATAGRID_HEADER_HEIGHT}
    disableExtendRowFullWidth={false}
    autoHeight
    density="standard"
    pageSize={PAGE_SIZE}
    {...props}
    ref={ref}
  />
);

DataGrid.displayName = 'DataGrid';

export default forwardRef(DataGrid);
