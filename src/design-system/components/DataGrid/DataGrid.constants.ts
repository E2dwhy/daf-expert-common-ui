import { GridLocaleText } from '@mui/x-data-grid';

export const DATAGRID_HEADER_HEIGHT = 54;

export const PAGE_SIZE = 100;

export const getFooterRowSelected = (count: number) => {
  return count === 1 ? `${count} ligne sélectionnée` : `${count} lignes sélectionnées`;
};
export const localText: Partial<GridLocaleText> | undefined = {
  footerRowSelected: getFooterRowSelected,
  noRowsLabel: 'Pas de données',
  MuiTablePagination: {
    labelRowsPerPage: 'Lignes par page',
    labelDisplayedRows: (paginationInfos) =>
      `${paginationInfos.from}-${paginationInfos.to} sur ${paginationInfos.count}`,
  },
};

export const pxToRem = (value: number) => `${value / 16}rem`;
