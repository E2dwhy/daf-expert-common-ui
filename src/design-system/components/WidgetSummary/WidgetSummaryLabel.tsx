import { Grid } from '@mui/material';
import React from 'react';
import { Text } from '../Text';

export default function WidgetSummaryLabel({ name }: { name: string }) {
  return (
    <Grid item xs={7} md={7}>
      <Text level="subtitle">{name.toLowerCase()}</Text>
    </Grid>
  );
}
