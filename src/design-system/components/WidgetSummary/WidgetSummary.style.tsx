import { Paper, Stack, styled } from '@mui/material';

export const StyledImgContainer = styled(Stack)({
  width: '60px',
  height: '60px',
  borderRadius: '50%',
  overflow: 'hidden',
  mt: 1.5,
});

export const StyledImg = styled('img')({
  width: '60%',
  height: '60%',
  margin: '10px auto',
});

export const StyledPaper = styled(Paper)({
  height: '100%',
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
});
