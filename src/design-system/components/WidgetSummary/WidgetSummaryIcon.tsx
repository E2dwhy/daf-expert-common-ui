import React from 'react';

import { alpha, Grid } from '@mui/material';
import { StyledImg, StyledImgContainer } from './WidgetSummary.style';
import { IconProps } from './type';
import { imageUrl } from '@/utils';

const WidgetSummaryIcon = ({ data, icon, label, color }: IconProps) => {
  return (
    <Grid item xs={5} md={5}>
      <StyledImgContainer
        spacing={2}
        sx={{
          backgroundColor: alpha(color, 0.15),
        }}
      >
        <StyledImg
          style={{
            color: color,
          }}
          src={imageUrl({ label, icon, data })}
          alt="icon"
        />
      </StyledImgContainer>
    </Grid>
  );
};

export default WidgetSummaryIcon;
