import { Grid } from '@mui/material';
import React from 'react';

export default function WidgetSummaryValue({ total }: { total: string }) {
  return (
    <Grid item xs={12} md={12}>
      <strong>{total}</strong>
    </Grid>
  );
}
