export type TopBlockIcon = {
  label: string;
  icon: string;
  color: string;
};

export type WidgetSummaryProps<T> = {
  data: T[];
  icons: TopBlockIcon[];
};
export type DataMapProps = { name: string; type: string; total: string };

type DataType = { type: string };
export type IconProps = {
  data: DataType;
  icon: string;
  label: string;
  color: string;
};
