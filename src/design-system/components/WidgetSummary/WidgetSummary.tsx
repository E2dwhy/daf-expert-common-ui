import { getTotalValue } from '../../utils';
import { Grid } from '@mui/material';
import { DataMapProps, WidgetSummaryProps } from './type';
import { StyledPaper } from './WidgetSummary.style';
import WidgetSummaryIcon from './WidgetSummaryIcon';
import WidgetSummaryLabel from './WidgetSummaryLabel';
import WidgetSummaryValue from './WidgetSummaryValue';

const WidgetSummary = <T extends DataMapProps>({ data, icons }: WidgetSummaryProps<T>) => {
  return (
    <>
      {data?.map((values: T, key: number) => (
        <Grid item key={key} md sx={{ minWidth: 180 }}>
          <StyledPaper sx={{ p: 1 }}>
            <Grid container spacing={2}>
              <WidgetSummaryLabel name={values?.name} />

              <WidgetSummaryIcon
                data={values}
                icon={icons[key].icon}
                label={icons[key].label}
                color={icons[key].color}
              />

              <WidgetSummaryValue total={getTotalValue(values)} />
            </Grid>
          </StyledPaper>
        </Grid>
      ))}
    </>
  );
};

export default WidgetSummary;
