import { Grid } from '@mui/material';
import { ImageBackgroundProps } from './ImageBackground.types';

const ImageBackground = ({ backgroundUrl, children }: ImageBackgroundProps) => (
  <Grid
    item
    xs={false}
    md={6}
    sx={{
      backgroundImage: `url(${backgroundUrl})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    {children}
  </Grid>
);

export default ImageBackground;
