import { ReactNode } from 'react';

export type ImageBackgroundProps = {
  backgroundUrl: string;
  children: ReactNode;
};
