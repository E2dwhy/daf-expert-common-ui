import localforage from 'localforage';

type Profile = {
  id: string;
  accessToken: string;
  idToken?: string;
  connected?: boolean;
  structure: {
    firmName: string;
    picture: string;
    socialReason?: string;
  };
};

type User = {
  id?: string;
  currentProfileId: string;
  accessToken?: string;
  connected?: boolean;
  firstName?: string;
  lastName?: string;
  email?: string;
  hasOnlyOneProfile: boolean;
  hasMoreThanOneProfile: boolean;
  isAuthenticated: boolean;
  profiles: Profile[];
  structure?: {
    firmName: string;
    picture: string;
  };
};

const storage = {
  getAccessToken: async () => (await localforage.getItem<string>('accessToken')) || '',
  setAccessToken: (accessToken: string) => localforage.setItem('accessToken', accessToken),
  removeAccessToken: () => localforage.removeItem('accessToken'),

  getIdToken: async () => (await localforage.getItem<string>('__dafexpert_profile_token')) || '',

  setIdToken: (accessToken: string) =>
    localforage.setItem('__dafexpert_profile_token', accessToken),
  removeIdToken: () => localforage.removeItem('__dafexpert_profile_token'),

  getAccount: async () => await localforage.getItem<User>('account'),
  setAccount: async (account: Profile) => localforage.setItem('account', account),
  removeAccount: () => localforage.removeItem('account'),
};

export default storage;
