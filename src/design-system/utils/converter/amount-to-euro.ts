export const convertAmountToEuro = (amount: number) => {
  const options = { style: 'currency', currency: 'EUR' };
  return amount ? new Intl.NumberFormat('de-DE', options).format(amount) : 'N/A';
};
