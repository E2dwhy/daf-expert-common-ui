type ImgUrl = {
  data: { type: string };
  label: string;
  icon: string;
};

export const imageUrl = ({ data, label, icon }: ImgUrl) => {
  if (label === data?.type) {
    return icon;
  }
  return '';
};

export const getTotalValue = (values: { total: string; name: string }) =>
  values?.name?.includes('TAUX') ? values?.total + ' % ' : values?.total;
