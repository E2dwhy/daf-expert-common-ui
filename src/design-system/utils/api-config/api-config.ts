import axios from 'axios';
import { idxDBStorage } from '../storage';

export const config = {
  CORE: 'https://staging-core.dafexpert.com',
  AUTH: 'https://staging-authentification.dafexpert.com',
  FACTURATION: 'https://staging-facturation.dafexpert.com',
  ACCOUNTING_PLAN: 'https://staging-accounting-chart.dafexpert.com',
  UPLOAD_FILE: 'https://staging-upload.dafexpert.com',
  IMPORT_DOSSIER: 'https://staging-importdossier.dafexpert.com',
  COMPTABILITE: 'https://staging-api-macompta.dafexpert.com',
};

export type Source = keyof typeof config;

const apiConfig = {
  get: async <T>(source: Source, url: string, params?: object, headers?: any) => {
    const profile_token = await idxDBStorage.getIdToken();

    return axios.get<T>(url, {
      baseURL: config[source],
      headers: {
        ...headers,
        Authorization: `Bearer ${profile_token}`,
      },
      ...params,
    });
  },
  post: async <T>(source: Source, url: string, data: any, headers?: any) => {
    const profile_token = await idxDBStorage.getIdToken();

    return axios.post<T>(url, data, {
      baseURL: config[source],
      headers: { ...headers, Authorization: `Bearer ${profile_token}` },
    });
  },
  put: async <T>(source: Source, url: string, data: any, headers?: any) => {
    const profile_token = await idxDBStorage.getIdToken();

    return axios.put<T>(url, data, {
      baseURL: config[source],
      headers: { ...headers, Authorization: `Bearer ${profile_token}` },
    });
  },
  delete: async <T>(source: Source, url: string, headers?: any) => {
    const profile_token = await idxDBStorage.getIdToken();

    return axios.delete<T>(url, {
      baseURL: config[source],
      headers: { ...headers, Authorization: `Bearer ${profile_token}` },
    });
  },
};

export default apiConfig;
