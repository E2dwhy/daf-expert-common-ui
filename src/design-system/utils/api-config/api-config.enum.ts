export const enum SOURCE_API {
  CORE = 'CORE',
  AUTH = 'AUTH',
  FACTURATION = 'FACTURATION',
  ACCOUNTING_PLAN = 'ACCOUNTING_PLAN',
  UPLOAD_FILE = 'UPLOAD_FILE',
  IMPORT_DOSSIER = 'IMPORT_DOSSIER',
  COMPTABILITE = 'COMPTABILITE',
}
