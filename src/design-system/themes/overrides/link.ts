import { Components, LinkProps, Theme } from '@mui/material';
import { Link } from '../../components/Link';

type LinkOverridesOptions = () => {
  MuiLink: Components<Theme>['MuiLink'];
};

const link: LinkOverridesOptions = () => ({
  MuiLink: {
    defaultProps: {
      component: Link,
      underline: 'none',
    } as LinkProps,
    styleOverrides: {
      root: () => ({
        textTransform: 'none',
        color: 'black',
        '&:hover': {},
      }),
    },
  },
});

export default link();
