import { PaletteOptions } from '@mui/material/styles/createPalette';

type PaletteMode = 'default';

type ColorOptions = 'lighter' | 'light' | 'main' | 'dark' | 'darker';

type Color = Record<ColorOptions, string>;

type PaletteColorOptions =
  // | 'common'
  'primary';
/*   | 'secondary'
| 'info'
| 'success'
| 'warning'
| 'error'*/
type PaletteColor = Record<PaletteColorOptions, Color>;

//type PaletteOverridesOptions = ({name}: {name: string}) => PaletteOptions;

/*const palette: PaletteOverridesOptions = () => {
    return {
        primary: {
            main: '#FFF',
        },
        text: {
            primary: '#5F6368',
        },
    };
};*/

const palette: Record<PaletteMode, PaletteOptions> = {
  default: {
    primary: {
      light: '#339F8F',
      main: '#008773',
      dark: '#005E50',
      contrastText: '#fff',
    },
    secondary: {
      light: '#F4FEF7',
      main: '#F2FEF6',
      dark: '#A9B1AC',
      contrastText: '#545454',
    },
  },
  /*    digitexpert: {},
        declarons: {},
        comptabilite: {},
        eboot: {},
        ged: {}*/
};

export default palette;
