const appModules: { [k: string]: string } = {
  digitexpert_module: 'digit-expert',
  declarons: 'declarons',
  macompta_module: 'comptabilite',
  pgf_module: 'pgf',
  rh: 'rh',
  mn: 'mn',
  ecollect: 'ecollect',
  eboot: 'eboot',
  folders: 'folders',
  collaborators: 'collaborators',
  facturation: 'facturation',
  dafexpert: 'dafexpert',
};

export default appModules;
