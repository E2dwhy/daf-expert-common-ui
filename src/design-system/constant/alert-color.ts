export const {
  ERROR_TYPE_ERROR,
  ERROR_TYPE_WARNING,
  ERROR_TYPE_INFO,
  ERROR_TYPE_SUCCESS,
  COLOR_ERROR,
  COLOR_WARNING,
  COLOR_INFO,
} = {
  ERROR_TYPE_ERROR: 'error',
  ERROR_TYPE_WARNING: 'warning',
  ERROR_TYPE_INFO: 'info',
  ERROR_TYPE_SUCCESS: 'success',
  COLOR_ERROR: '#ef5350',
  COLOR_WARNING: 'rgba(255, 152, 0)',
  COLOR_INFO: 'rgba(3, 169, 244)',
};

export enum ErrorType {
  ERROR = 'error',
  WARNING = 'warning',
  INFO = 'info',
  SUCCESS = 'success',
}
