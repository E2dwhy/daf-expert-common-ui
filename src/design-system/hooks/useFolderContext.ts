import { useContext } from 'react';
import { DossierContext } from '../contexts';

export const useFolderContext = () => {
  const context = useContext(DossierContext);

  if (!context) throw new Error('Dossier context must be use inside DossierProvider');

  return context;
};
