import {
  COLOR_ERROR,
  COLOR_INFO,
  COLOR_WARNING,
  ErrorType,
  ERROR_TYPE_ERROR,
  ERROR_TYPE_INFO,
  ERROR_TYPE_SUCCESS,
  ERROR_TYPE_WARNING,
} from '../constant';
import { AdjustRounded } from '@mui/icons-material';

import React from 'react';

export default function useAlertIconColor() {
  type Props = {
    errorType: ErrorType;
  };

  const getColorIcon = ({ errorType }: Props) => {
    if (errorType) {
      const { color, marginRight } = { color: COLOR_ERROR, marginRight: 1 };

      switch (errorType) {
      case ERROR_TYPE_ERROR:
        return <AdjustRounded sx={{ color, marginRight }} />;
      case ERROR_TYPE_WARNING:
        return <AdjustRounded sx={{ color: COLOR_WARNING, marginRight }} />;
      case ERROR_TYPE_INFO:
        return <AdjustRounded sx={{ color: COLOR_INFO, marginRight }} />;
      case ERROR_TYPE_SUCCESS:
        return '';
      default:
        return (() => {
          throw new Error(`Invalid error type: ${errorType}`);
        })();
      }
    }
  };

  return { getColorIcon };
}
