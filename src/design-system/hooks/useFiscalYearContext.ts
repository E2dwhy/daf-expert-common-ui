import { useContext } from 'react';
import { FiscalYearContext } from '../contexts';

export const useFiscalYearContext = () => {
  const context = useContext(FiscalYearContext);

  if (!context) throw new Error('FiscalYear context must be use inside FiscalYearProvider');

  return context;
};
