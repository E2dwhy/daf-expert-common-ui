import { useState } from 'react';

// ----------------------------------------------------------------------

/**
 * hook to manage tabs
 * @param defaultValues string
 * @returns { currentTab: string, onChangeTab: (event: any, newValue: any) => void, setCurrentTab: Dispatch<SetStateAction<string>> }
 */
export const useTabs = (defaultValues: string) => {
  const [currentTab, setCurrentTab] = useState(defaultValues || '');

  return {
    currentTab,
    onChangeTab: (event: any, newValue: any) => {
      setCurrentTab(newValue);
    },
    setCurrentTab,
  };
};
