import { useContext } from 'react';
import { ConsoleContext } from '../contexts';

export const useConsoleContext = () => {
  const context = useContext(ConsoleContext);

  if (!context) throw new Error('Console context must be use inside ConsoleProvider');

  return context;
};
