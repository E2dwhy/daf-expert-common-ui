import { useContext } from 'react';
import { ThemeContext } from '../themes';

export const useAppTheme = () => {
  const context = useContext(ThemeContext);

  if (!context) throw new Error('Theme context must be use inside ThemeProvider');

  return context;
};
