import { createContext, Dispatch, ReactNode, SetStateAction, useMemo, useState } from 'react';
import { useConsoleContext } from '../../hooks';

export type FiscalYear = {
  year: string;
};

type FiscalYearContextProps = {
  topBarDialogShouldBeEnabled: boolean;
  openFiscalYearTopBarDialog: boolean;
  setOpenFiscalYearTopBarDialog: Dispatch<SetStateAction<boolean>>;
  fiscalYear: null | FiscalYear;
  setFiscalYear: Dispatch<SetStateAction<FiscalYear>>;
  fiscalYearQueryParam: string;
};

const fiscalYearContextInitialValues = {
  topBarDialogShouldBeEnabled: false,
  openFiscalYearTopBarDialog: false,
  setOpenFiscalYearTopBarDialog: () => undefined,
  fiscalYear: {
    year: '',
  },
  setFiscalYear: () => undefined,
  fiscalYearQueryParam: '',
};

export const FiscalYearContext = createContext<FiscalYearContextProps>(
  fiscalYearContextInitialValues
);

type FiscalYearProviderProps = {
  children: ReactNode;
};

export const FiscalYearProvider = ({ children }: FiscalYearProviderProps) => {
  /*State for open/clos fiscal year dialog open from top bar */
  const [openFiscalYearTopBarDialog, setOpenFiscalYearTopBarDialog] = useState(
    fiscalYearContextInitialValues.openFiscalYearTopBarDialog
  );
  /*State for fiscal year. It should use to update or get fiscal year everywhere*/
  const [fiscalYear, setFiscalYear] = useState(fiscalYearContextInitialValues.fiscalYear);

  const { fiscalYearQueryParam, moduleSelected } = useConsoleContext();

  useMemo(() => {
    setFiscalYear({ year: fiscalYearQueryParam });
  }, [fiscalYearQueryParam]);

  return (
    <FiscalYearContext.Provider
      value={{
        topBarDialogShouldBeEnabled: moduleSelected.shouldSelectFiscalYear,
        fiscalYear,
        setFiscalYear,
        openFiscalYearTopBarDialog,
        setOpenFiscalYearTopBarDialog,
        fiscalYearQueryParam,
      }}
    >
      {children}
    </FiscalYearContext.Provider>
  );
};
