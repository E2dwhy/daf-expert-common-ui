import { SOURCE_API, useFetch } from '../../utils';
import { createContext, ReactNode, useMemo, useState } from 'react';
import { useSearchParams } from 'react-router-dom';

const useGetAccountantsModules = () => {
  return useFetch<{ data: any[] }>(SOURCE_API.FACTURATION, '/accountants/modules/structures');
};

type ConsoleProviderProps = {
  children: ReactNode;
};

export type Module = {
  name: string;
  code: string;
  features: {
    name: string;
    code: string;
    enabled: boolean;
  }[];
};

const consoleProperties: ConsolePropertiesProps = {
  modules: [],
  dafModules: [],
  moduleSelected: {
    name: '',
    shouldSelectDossiers: true,
    shouldSelectFiscalYear: true,
  },
  dossierQueryParam: '',
  fiscalYearQueryParam: '',
};

type ConsolePropertiesProps = {
  modules: Module[];
  dafModules: Module[];
  moduleSelected: {
    name: string;
    shouldSelectDossiers: boolean;
    shouldSelectFiscalYear: boolean;
  };
  dossierQueryParam: string;
  fiscalYearQueryParam: string;
};

export const ConsoleContext = createContext<ConsolePropertiesProps>(consoleProperties);

export const ConsoleProvider = ({ children }: ConsoleProviderProps) => {
  const [qParam] = useSearchParams();
  const [dossierQueryParam, setDossierQueryParam] = useState<string>('');
  const [fiscalYearQueryParam, setFiscalYearQueryParam] = useState<string>('');
  const [modules, setModules] = useState<Module[]>([]);
  const [dafModules, setDafModules] = useState<any[]>([]);
  const { data: accountantsModules } = useGetAccountantsModules();

  useMemo(() => {
    setDossierQueryParam(qParam.get('dossier') || '');
    setFiscalYearQueryParam(qParam.get('year') || '');
    setDafModules(
      accountantsModules?.data?.filter(
        (item: { code: string }) => item.code === 'DAFEXPERT_MODULE'
      )[0].features ?? []
    );
    setModules(
      accountantsModules?.data?.filter(
        (item: { code: string }) => item.code !== 'DAFEXPERT_MODULE'
      ) ?? []
    );
  }, [accountantsModules?.data, qParam]);

  return (
    <ConsoleContext.Provider
      value={{
        modules,
        dafModules,
        moduleSelected: consoleProperties.moduleSelected,
        dossierQueryParam,
        fiscalYearQueryParam,
      }}
    >
      {children}
    </ConsoleContext.Provider>
  );
};
