import { useConsoleContext } from '../../hooks';
import { createContext, Dispatch, ReactNode, SetStateAction, useMemo, useState } from 'react';

type Dossier = {
  id: string;
  code: string;
  socialReason: string;
  identificationNumber: string;
  status: string;
};

type DossierContextProps = {
  topBarDialogShouldBeEnabled: boolean;
  openTopBarDialog: boolean;
  setOpenTopBarDialog: Dispatch<SetStateAction<boolean>>;
  dossier: Dossier;
  setDossier: Dispatch<SetStateAction<Dossier>>;
};

const dossierContextInitialValues = {
  topBarDialogShouldBeEnabled: true,
  setTopBarDialogShouldBeEnabled: () => undefined,
  openTopBarDialog: false,
  setOpenTopBarDialog: () => undefined,
  dossier: {} as Dossier,
  setDossier: () => undefined,
};

export const DossierContext = createContext<DossierContextProps>(dossierContextInitialValues);

type DossierProvideProps = {
  children: ReactNode;
};

export const DossierProvider = ({ children }: DossierProvideProps) => {
  const [openTopBarDialog, setOpenTopBarDialog] = useState<boolean>(
    dossierContextInitialValues.openTopBarDialog
  );

  const [dossier, setDossier] = useState<Dossier>(dossierContextInitialValues.dossier);

  const { dossierQueryParam, moduleSelected } = useConsoleContext();

  useMemo(() => {
    setDossier({ id: dossierQueryParam } as Dossier);
  }, [dossierQueryParam]);

  return (
    <DossierContext.Provider
      value={{
        dossier,
        setDossier,
        openTopBarDialog,
        setOpenTopBarDialog,
        topBarDialogShouldBeEnabled: moduleSelected.shouldSelectDossiers,
      }}
    >
      {children}
    </DossierContext.Provider>
  );
};
